﻿using PhoneBook.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using System.Xml;

namespace PhoneBook.Data
{
    public class PersonData
    {
        //the path to phonebook file
        static string path = AppDomain.CurrentDomain.BaseDirectory + "Content\\Phonebook.xml";

        public static bool AddPersonToPhonebook(Person person)
        {
            List<Person> list = new List<Person>();
            list = ReadPhonebookFile();
            list.Add(person);
            WritePhonebookToFile(list);
            return true;
        }

        public static List<Person> ReadPhonebookFile()
        {
            List<Person> deserializedPersonList = new List<Person>();
            FileStream filestream = new FileStream(path,FileMode.OpenOrCreate);
            //if file not empty
            if (filestream.Length > 0)
            {
                //deserialize data from file
                XmlDictionaryReader reader = XmlDictionaryReader.CreateTextReader(filestream, new XmlDictionaryReaderQuotas());
                DataContractSerializer serializer = new DataContractSerializer(typeof(List<Person>));
                deserializedPersonList = new List<Person>();
                deserializedPersonList = (List<Person>)serializer.ReadObject(reader, true);
                reader.Close();
            }
            filestream.Close();
            return deserializedPersonList;
        }

        public static void WritePhonebookToFile(List<Person> list)
        {
            //write serialized data to file
            FileStream writer = new FileStream(path, FileMode.Truncate);
            DataContractSerializer serializer = new DataContractSerializer(typeof(List<Person>));
            serializer.WriteObject(writer, list);
            writer.Close();
        }
    }
}