﻿using PhoneBook.Data;
using PhoneBook.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PhoneBook.Controllers
{
    public class HomeController : Controller
    {
        //main page
        public ActionResult Index()
        {
            List<Person> list;
            list = PersonData.ReadPhonebookFile();
            return View(list);
        }

        //main page with sorting
        [HttpGet]
        public ActionResult Index(string SortBy="")
        {
            List<Person> list;
            list = PersonData.ReadPhonebookFile();
            //select the type of sorting
            switch (SortBy)
            {
                case "BirthDate": list = list.OrderBy(x=>x.BirthYear).ToList(); break;
                case "Surname": list = list.OrderBy(x => x.Surname).ToList(); break;
            }
            return View(list);
        }

        //the page for adding person
        public ActionResult AddPerson()
        {
            Person person = new Person();
            return View(person);
        }

        [HttpPost]
        public ActionResult AddPerson(Person person)
        {
            PersonData.AddPersonToPhonebook(person);
            return RedirectToAction("Index");
        }

        //method for deleting person
        [HttpGet]
        public ActionResult DeletePerson(string personNumber)
        {
            List<Person> list;
            list = PersonData.ReadPhonebookFile();
            list.RemoveAll(x => x.PhoneNumber == personNumber);
            PersonData.WritePhonebookToFile(list);
            return RedirectToAction("Index");
        }

        //the page with search results
        [HttpPost]
        public ActionResult SearchResult(string searchText)
        {
            List<Person> list;
            list = PersonData.ReadPhonebookFile();
            //transfer data to page using Veiwbag
            ViewBag.SearchBySurname = list.FindAll(x => x.Surname.Contains(searchText));
            ViewBag.SearchByName = list.FindAll(x => x.Name.Contains(searchText));
            ViewBag.SearchByNumber = list.FindAll(x => x.PhoneNumber.Contains(searchText));
            return View();
        }
    }
}