﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace PhoneBook.Models
{
    //The class describes all parameters of the person in the notebook
    [DataContract]
    public class Person
    {
        private static int MinBirthYear = 1890;
        private static int MaxBirthYear = DateTime.Now.Year;
        private int _BirthYear;

        [Display(Name = "Name", ResourceType = typeof(Properties.Resources))]
        [DataMember]
        public string Name { get; set; }

        [Display(Name = "Surname", ResourceType = typeof(Properties.Resources))]
        [DataMember]
        public string Surname { get; set; }

        [Display(Name = "BirthYear", ResourceType = typeof(Properties.Resources))]
        [DataMember]
        public int BirthYear
        {
            get
            {
                return _BirthYear;
            }
            set
            {
                if ((value >= MinBirthYear && value <= MaxBirthYear) || value == 0) { _BirthYear = value; }
            }
        }

        [Display(Name = "PhoneNumber", ResourceType = typeof(Properties.Resources))]
        [DataMember]
        public string PhoneNumber { get; set; }

        public Person()
        {
            Name = "";
            Surname = "";
            BirthYear = 0;
            PhoneNumber = "";
        }

        public Person(string name ="", string surname = "", int birthyear = 0, string phonenumber = "")
        {
            Name = name;
            Surname = surname;
            BirthYear = birthyear;
            PhoneNumber = phonenumber;
        }
    }
}